var loginDefinitionsWrapper = function () {
  this.url = '';
  this.errorMsg = '';
 
  this.World = require("../support/jabbakam.js").World; 


  this.Given(/^I am not logged in$/, function(callback) {
    // nothing to do here, possibly delete cookies?
    callback();
  });

  this.When(/^I go to the "([^"]*)" page$/, function(arg1, callback) {
    var self = this;
    var pageUrl = getPageUrl(arg1);
    this.visit(pageUrl, function(url) {
      self.url = url;
      callback();
    });
  });
  
  this.When(/^I enter incorrect login credentials$/, function(callback) {
    var self = this;
    var email = 'test@email.com';
    var pass = 'wrong password';
    // this is horrible copy/paste, but could easily be abstracted away
    this.login(email, pass, function(url, errorMsg) {
      self.url = url;
      self.errorMsg = errorMsg;
      callback();
    });
  });
  
  this.When(/^I enter my correct login credentials$/, function(callback) {
    var self = this;
    var email = 'test@email.com';
    var pass = 'password';
    // this is horrible copy/paste, but could easily be abstracted away
    this.login(email, pass, function(url, errorMsg) {
      self.url = url;
      self.errorMsg = errorMsg;
      callback();
    });
  });

  this.Then(/^I should see the "([^"]*)" error message$/, function(arg1, callback) {
    if (this.errorMsg != arg1) {
      return callback.fail();
    }
    callback();
  });

  this.Then(/^I should be on the "([^"]*)" page$/, function(arg1, callback) {
    var pageUrl = getPageUrl(arg1);

    if(this.url != pageUrl) {
      return callback.fail();
    }

    callback();
  });


  // this is abstracting away the urls, so business people can understand easier
  var getPageUrl = function(pageName) {
    if(pageName == 'Login') {
      return 'http://jk.dev/?ref=';
    }
    if(pageName == 'Login for Inbox') {
      return 'http://jk.dev/?ref=inbox';
    }
    if(pageName == 'Dashboard') {
      return 'http://jk.dev/home';
    }
    if(pageName == 'Inbox') {
      return 'http://jk.dev/inbox';
    }
  };

};

module.exports = loginDefinitionsWrapper;
