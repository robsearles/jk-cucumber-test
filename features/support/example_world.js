var Browser = require('zombie');
var World = function World(callback) {
  this.browser = new Browser(); 


  this.visit = function(url, callback) {
    this.browser.visit(url, callback);
  };

  callback(); // tell Cucumber we're finished and to use 'this' as the world instance
};
exports.World = World;
