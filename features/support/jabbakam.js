var Browser = require("zombie");

var World = function World(callback) {
  // Load the page from localhost
  this.browser = new Browser({
    debug: false,
    maxWait: 600,
    maxRedirects: 30,
    runScripts: false,
    loadCSS: false,
  });


  this.visit = function(url, callback) {
    var self = this;
    this.browser.visit(url, function() {
      var url = self.browser.url;
      callback(url);
    })
  };

  this.login = function(email, pass, callback) {
    var self = this;
    this.browser.
      fill("email", email).
      fill("password", pass).
      pressButton("Login", function() {      
        var url = self.browser.url;
        var errorMsg = self.browser.text("#login-form p.error");
        callback(url, errorMsg);
    });
  };
  
  callback();
};


exports.World = World;
