# Login Feature

Feature: Login to Jabbakam
  As a user of Jabbakam
  I want to be able to successfully login
  So I can use the Jabbakam Website functionality

  Scenario: Incorrect login credentials supplied
    Given I am not logged in
    When I go to the "Login" page
    And I enter incorrect login credentials
    Then I should see the "Login details are incorrect." error message

  Scenario: Correct login credentials supplied
    Given I am not logged in
    When I go to the "Login" page
    And I enter my correct login credentials
    Then I should be on the "Dashboard" page

  Scenario: Redirect to login page when not logged in
    Given I am not logged in
    When I go to the "Inbox" page
    Then I should be on the "Login for Inbox" page

  Scenario: Redirect to the Inbox page after logging in
    Given I am not logged in
    When I go to the "Login for Inbox" page
    And I enter my correct login credentials
    Then I should be on the "Inbox" page
