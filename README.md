# Example Features Tests

I have quickly created some example tests using the Gherkin behaviour testing syntax.

This is a simple example, but can hopefully demonstrate the possibilities of what can be done.

The idea behind this is to be able to set up a complete testing environment, which will test the behaviour of the entire system. This can be accomplished by creating simulated version of the VNA, centrix, ZMQ system etc.


For example, you could have a test that makes a simulated Centrix API call to the system, setting the FPS for a VNA device to 10 FPS. The simulated VNA device which has been attached then listens to see if it has received the correct message from the server.

The testing scenario may look like this:

```
  Scenario: Setting event frame rate to 10 fps
    Given I am using the Centrix API to configure device "one"
    When I set the event frame rate to "10"
    Then VNA device "one" should receive the message "Event FPS" value "10"
```

You could even add an "And VNA device "two" should not receive the message "e_fr:10\n" " if you want?!

## Usage

```
cucumber.js features/login.feature 
```

## Requirements

You need cucumber-js (install globally is best) and zombie.js (installed locally as a node module)

* https://github.com/cucumber/cucumber-js
* http://zombie.labnotes.org/

### Useful Links

* https://github.com/cucumber/cucumber/wiki/Gherkin
* https://github.com/cucumber/cucumber/wiki/Given-When-Then
* https://github.com/cucumber/cucumber/wiki/Feature-Introduction

## Key Files

* features/login.feature
* features/step_definitions/login.js
* features/support/jabbakam.js

## Notes

I have set up the environment to be my local development machine. You will need to change settings in "features/support/jabbakam.js"


